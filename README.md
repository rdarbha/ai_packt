# AI_packt

This repo is my personal learnings whilc completing the activities identified in Artificial Intelligence with Python book, written by Prateek Joshi and published by Packt.


The learnings are all in python, and require the following packages:
```
$ pip freeze
cycler==0.10.0
joblib==0.15.1
kiwisolver==1.2.0
matplotlib==3.2.1
numpy==1.18.4
pyparsing==2.4.7
python-dateutil==2.8.1
scikit-learn==0.23.1
scipy==1.4.1
six==1.15.0
threadpoolctl==2.1.0
```

All dependencies can be installed using the following packages through pip:
```
pip install numpy
pip install scipy
pip install scikit-learn
pip install matplotlib
```

I've personally used virtual env and pip to install all the above packages to avoid package dependency issues.


To perform the steps for Logistic Regression, if you're using an Ubuntu computer for this learning, you'll need to install `pythonr3-tk` so that the matplotlib backend uses `TkAgg` instead of `agg`.
Use the steps outlined here:
https://www.pyimagesearch.com/2015/08/24/resolved-matplotlib-figures-not-showing-up-or-displaying/

```
(AI) rama@Computer:~/git/ai_packt$ python
Python 3.6.9 (default, Apr 18 2020, 01:56:04)
[GCC 8.4.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import matplotlib
>>> matplotlib.get_backend()
'agg'
>>>
```

```
(AI) rama@Computer:~/git/ai_packt$ sudo apt-get install python3-tk
[sudo] password for rama:
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following additional packages will be installed:
  blt tk8.6-blt2.5
Suggested packages:
  blt-demo tix python3-tk-dbg
The following NEW packages will be installed:
  blt python3-tk tk8.6-blt2.5
0 upgraded, 3 newly installed, 0 to remove and 24 not upgraded.
Need to get 696 kB of archives.
After this operation, 3,350 kB of additional disk space will be used.
Do you want to continue? [Y/n] Y
Get:1 http://us.archive.ubuntu.com/ubuntu bionic/main amd64 tk8.6-blt2.5 amd64 2.5.3+dfsg-4 [572 kB]
Get:2 http://us.archive.ubuntu.com/ubuntu bionic/main amd64 blt amd64 2.5.3+dfsg-4 [4,944 B]
Get:3 http://us.archive.ubuntu.com/ubuntu bionic-updates/main amd64 python3-tk amd64 3.6.9-1~18.04 [119 kB]
Fetched 696 kB in 0s (2,174 kB/s)
Selecting previously unselected package tk8.6-blt2.5.
(Reading database ... 278487 files and directories currently installed.)
Preparing to unpack .../tk8.6-blt2.5_2.5.3+dfsg-4_amd64.deb ...
Unpacking tk8.6-blt2.5 (2.5.3+dfsg-4) ...
Selecting previously unselected package blt.
Preparing to unpack .../blt_2.5.3+dfsg-4_amd64.deb ...
Unpacking blt (2.5.3+dfsg-4) ...
Selecting previously unselected package python3-tk:amd64.
Preparing to unpack .../python3-tk_3.6.9-1~18.04_amd64.deb ...
Unpacking python3-tk:amd64 (3.6.9-1~18.04) ...
Setting up tk8.6-blt2.5 (2.5.3+dfsg-4) ...
Setting up blt (2.5.3+dfsg-4) ...
Setting up python3-tk:amd64 (3.6.9-1~18.04) ...
Processing triggers for libc-bin (2.27-3ubuntu1) ...
```

```
(AI) rama@Computer:~/git/ai_packt$ python3
Python 3.6.9 (default, Apr 18 2020, 01:56:04)
[GCC 8.4.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import matplotlib
>>> matplotlib.get_backend()
'TkAgg'
```




## Open Issues

### Chapter 2 - SVM errors

When I run the Support Vector Machine code, I get the following warning:
```
/home/rama/venv/AI/lib/python3.6/site-packages/sklearn/svm/_base.py:977: ConvergenceWarning: Liblinear failed to converge, increase the number of iterations.
  "the number of iterations.", ConvergenceWarning)
```

This is not good as it means my linearSVC has not converged properly for some reason. And without a properly convering algorithm, the results of the classifier can be garbage.


In addition, the F1 score of should be `66.82%` as per the book.

When I run the code exactly as is, I get an F1 score of `56.15%`.

If I increase the number of iterations using the following code (default is max_iter=1000):
```
classifier = OneVsOneClassifier(LinearSVC(random_state=0,max_iter=10000))
```

The F1 score becomes: 70.71%

Link to scikit linearSVC method: https://scikit-learn.org/stable/modules/generated/sklearn.svm.LinearSVC.html

This link gave me some guidance on increasing the max_iter and maybe even setting `dual = false`, but none of those options actually solve this problem.
https://stackoverflow.com/questions/52670012/convergencewarning-liblinear-failed-to-converge-increase-the-number-of-iterati

I can't help but feel maybe I have a garbage data issue?